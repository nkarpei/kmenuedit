# translation of kmenuedit.po to 
# This file is distributed under the same license as the PACKAGE package.
# Copyright (C) YEAR This_file_is_part_of_KDE.
# Mahesh <mahesh@pal.ece.iisc.ernet.in>, 2007.
#
msgid ""
msgstr ""
"Project-Id-Version: kmenuedit\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-07 00:48+0000\n"
"PO-Revision-Date: 2007-09-10 22:19+0530\n"
"Last-Translator: Mahesh <mahesh@pal.ece.iisc.ernet.in>\n"
"Language-Team:  <en@li.org>\n"
"Language: kn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.9.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "ವಿ ಕೆ ಮಹೇಶ"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "vkmahesh1231@rediffmail.com"

#: basictab.cpp:49
#, kde-format
msgid "&Name:"
msgstr "(&ಹ)ಹೆಸರು:"

#: basictab.cpp:58
#, kde-format
msgid "&Description:"
msgstr "ವಿ&ವರಣೆ:"

#: basictab.cpp:66
#, kde-format
msgid "&Comment:"
msgstr "(&ಟ)ಟಿಪ್ಪಣಿ:"

#: basictab.cpp:74
#, kde-format
msgctxt "@label:textfield"
msgid "Environment Variables:"
msgstr ""

#: basictab.cpp:81
#, kde-format
msgctxt "@label:textbox the name or path to a command-line program"
msgid "Program:"
msgstr ""

#: basictab.cpp:88
#, kde-format
msgctxt "@label:textfield"
msgid "Command-Line Arguments:"
msgstr ""

#: basictab.cpp:92
#, kde-format
msgid ""
"Following the command, you can have several place holders which will be "
"replaced with the actual values when the actual program is run:\n"
"%f - a single file name\n"
"%F - a list of files; use for applications that can open several local files "
"at once\n"
"%u - a single URL\n"
"%U - a list of URLs\n"
"%d - the folder of the file to open\n"
"%D - a list of folders\n"
"%i - the icon\n"
"%m - the mini-icon\n"
"%c - the caption"
msgstr ""
"ಆದೇಶವನ್ನು ಅನುಸರಿಸಿದರೆ, ನೀವು, ಕ್ರಮವಿಧಿ ಚಲಾವಣೆಯಾದಾಗ ನೈಜ ಮೌಲ್ಯಗಳಿಗೆ ಬದಲಾಯಿಸಲ್ಪಡುವ "
"ಅನೇಕ ಸ್ಥಾನ ಗ್ರಾಹಕಗಳನ್ನು ಪಡೆಯಬಹುದು:\n"
"%f - ಒಂದು ಕಡತದ ಹೆಸರು\n"
"%F - ಕಡತಗಳ ಪಟ್ಟಿ; ಅನೇಕ ಕಡತಗಳನ್ನು ಒಟ್ಟಿಗೇ ತೆರೆಯಬಲ್ಲ ಅನ್ವಯಗಳೊಂದಿಗೆ ಬಳಸಿ\n"
"%u - ಒಂದು ಜಾಲತಾಣ ಸೂಚಿ##(URL)## \n"
"%U - ಜಾಲತಾಣ ಸೂಚಿಗಳ ಪಟ್ಟಿ \n"
"%d - ತೆರೆಯಬೇಕಾದ ಕಡತದ ಕೋಶ \n"
"%D - ಕಡತ ಕೋಶಗಳ ಪಟ್ಟಿ\n"
"%i - ಲಾಂಛನ\n"
"%m - ಚಿಕ್ಕ ಲಾಂಛನ\n"
"%c - ಶಿರೋನಾಮೆ"

#: basictab.cpp:107
#, kde-format
msgid "Enable &launch feedback"
msgstr "ಪ್ರತಿಕ್ರಿಯಾ ಅನ್ವಯದ##(feedback)## ಪ್ರಕ್ಷೇ&ಪಕವನ್ನು ಸಚೇತಗೊಳಿಸು"

#: basictab.cpp:111
#, kde-format
msgid "Only show when logged into a Plasma session"
msgstr ""

#: basictab.cpp:115
#, kde-format
msgid "Hidden entry"
msgstr ""

#: basictab.cpp:127 preferencesdlg.cpp:63
#, kde-format
msgid "General"
msgstr ""

#: basictab.cpp:139
#, kde-format
msgid "&Work path:"
msgstr "ಹಾ(&ಲ)ಲಿ ಮಾರ್ಗ:"

#: basictab.cpp:151
#, kde-format
msgid "Run in term&inal"
msgstr "&ಗಣಕ ಘಟಕದಲ್ಲಿ ನಿರ್ವಹಿಸು"

#: basictab.cpp:156
#, kde-format
msgid "Terminal &options:"
msgstr "ಗಣಕ ಘಟಕದ ಆಯ್ಕೆ&ಗಳು:"

#: basictab.cpp:170
#, kde-format
msgid "&Run as a different user"
msgstr "ಇನ್ನೊಬ್ಬ ಬಳಕೆದಾರನಂತೆ &ಚಲಾಯಿಸು"

#: basictab.cpp:175
#, kde-format
msgid "&Username:"
msgstr "&ಬಳಕೆದಾರನ ಹೆಸರು:"

#: basictab.cpp:189
#, kde-format
msgid "Current shortcut &key:"
msgstr "ಹಾಲಿ ಶೀಘ್ರಮಾರ್ಗ (&ಕ)ಕೀಲಿ:"

#: basictab.cpp:200
#, kde-format
msgid "Advanced"
msgstr ""

#: globalaccel.cpp:24
#, kde-format
msgid "Launch %1"
msgstr ""

#: kmenuedit.cpp:55
#, kde-format
msgid "&New Submenu..."
msgstr "(&ಹ)ಹೊಸ ಉಪಪರಿವಿಡಿ..."

#: kmenuedit.cpp:59
#, kde-format
msgid "New &Item..."
msgstr "ಹೊಸ &ವಸ್ತು..."

#: kmenuedit.cpp:63
#, kde-format
msgid "New S&eparator"
msgstr "ಹೊಸ (&ವ)ವಿಯೋಜಕ"

#: kmenuedit.cpp:67
#, kde-format
msgid "&Sort"
msgstr ""

#: kmenuedit.cpp:71
#, fuzzy, kde-format
#| msgid "&Description:"
msgid "&Sort selection by Name"
msgstr "ವಿ&ವರಣೆ:"

#: kmenuedit.cpp:74
#, fuzzy, kde-format
#| msgid "&Description:"
msgid "&Sort selection by Description"
msgstr "ವಿ&ವರಣೆ:"

#: kmenuedit.cpp:78
#, kde-format
msgid "&Sort all by Name"
msgstr ""

#: kmenuedit.cpp:81
#, fuzzy, kde-format
#| msgid "&Description:"
msgid "&Sort all by Description"
msgstr "ವಿ&ವರಣೆ:"

#: kmenuedit.cpp:87
#, kde-format
msgid "Move &Up"
msgstr ""

#: kmenuedit.cpp:90
#, kde-format
msgid "Move &Down"
msgstr ""

#: kmenuedit.cpp:98
#, kde-format
msgid "Restore to System Menu"
msgstr ""

#: kmenuedit.cpp:126
#, kde-format
msgid "Search..."
msgstr ""

#: kmenuedit.cpp:127
#, kde-format
msgid "Search through the list of applications below"
msgstr ""

#: kmenuedit.cpp:188
#, kde-format
msgid "&Delete"
msgstr "ಅಳಿ(&ಸ)ಸು"

#: kmenuedit.cpp:213
#, kde-format
msgid ""
"You have made changes to the menu.\n"
"Do you want to save the changes or discard them?"
msgstr ""
"ನೀವು ಪರಿವಿಡಿಯಲ್ಲಿ ಬದಲಾವಣೆಗಳನ್ನು ಮಾಡಿದ್ದೀರಿ.\n"
"ಇವನ್ನು ನೀವು ಉಳಿಸಲು ಬಯಸುತ್ತೀರೋ ತಿರಸ್ಕರಿಸಲು ಬಯಸುತ್ತೀರೋ?"

#: kmenuedit.cpp:215
#, kde-format
msgid "Save Menu Changes?"
msgstr "ಪರಿವಿಡಿಯ ಬದಲಾವಣೆಗಳನ್ನು ಉಳಿಸಲೇ?"

#. i18n: ectx: Menu (file)
#: kmenueditui.rc:7
#, kde-format
msgid "&File"
msgstr "&ಕಡತ"

#. i18n: ectx: Menu (edit)
#: kmenueditui.rc:17
#, kde-format
msgid "&Edit"
msgstr "ಸಂಪಾ(&ದ)ದಿಸು"

#. i18n: ectx: ToolBar (mainToolBar)
#: kmenueditui.rc:34
#, kde-format
msgid "Main Toolbar"
msgstr "ಮುಖ್ಯ ಸಲಕರಣೆಪಟ್ಟಿ"

#: main.cpp:31 main.cpp:48 main.cpp:62
#, kde-format
msgid "KDE Menu Editor"
msgstr "ಕೆಡಿಇ ಪರಿವಿಡಿ ಸಂಪಾದಕ"

#: main.cpp:50
#, kde-format
msgid "KDE menu editor"
msgstr "ಕೆಡಿಇ ಪರಿವಿಡಿ ಸಂಪಾದಕ"

#: main.cpp:52
#, kde-format
msgid "(C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter"
msgstr "(C) 2000-2003, Waldo Bastian, Raffaele Sandrini, Matthias Elter"

#: main.cpp:53
#, kde-format
msgid "Waldo Bastian"
msgstr "Waldo Bastian"

#: main.cpp:53
#, kde-format
msgid "Maintainer"
msgstr "ಪಾಲಕ"

#: main.cpp:54
#, kde-format
msgid "Raffaele Sandrini"
msgstr "Raffaele Sandrini"

#: main.cpp:54
#, kde-format
msgid "Previous Maintainer"
msgstr "ಮುಂಚಿನ ಪಾಲಕ"

#: main.cpp:55
#, kde-format
msgid "Matthias Elter"
msgstr "Matthias Elter"

#: main.cpp:55
#, kde-format
msgid "Original Author"
msgstr "ಮೂಲ ಕರ್ತೃ"

#: main.cpp:56
#, kde-format
msgid "Montel Laurent"
msgstr ""

#: main.cpp:63
#, kde-format
msgid "Sub menu to pre-select"
msgstr "ಮುಂಚಿತ ಆಯ್ಕೆಯ ಉಪ ಪರಿವಿಡಿ"

#: main.cpp:64
#, kde-format
msgid "Menu entry to pre-select"
msgstr "ಮುಂಚಿತ ಆಯ್ಕೆಗಾಗಿ ಪರಿವಿಡಿ-ನಮೂದನೆ"

#: menufile.cpp:94 menufile.cpp:108
#, kde-format
msgid "Could not write to %1"
msgstr "%1 ಕಡತಕ್ಕೆ ಬರೆಯಲಾಗಲಿಲ್ಲ"

#: preferencesdlg.cpp:26
#, fuzzy, kde-format
#| msgid "Terminal &options:"
msgid "General options"
msgstr "ಗಣಕ ಘಟಕದ ಆಯ್ಕೆ&ಗಳು:"

#: preferencesdlg.cpp:31
#, kde-format
msgid "Spell Checking"
msgstr ""

#: preferencesdlg.cpp:32
#, kde-format
msgid "Spell checking Options"
msgstr ""

#: preferencesdlg.cpp:65
#, kde-format
msgid "Show hidden entries"
msgstr ""

#: treeview.cpp:177
#, kde-format
msgid " [Hidden]"
msgstr " [ಅಡಗಿಸಿದ]"

#: treeview.cpp:1018
#, kde-format
msgid "New Submenu"
msgstr "ಹೊಸ ಉಪಪರಿವಿಡಿ"

#: treeview.cpp:1018
#, kde-format
msgid "Submenu name:"
msgstr "ಉಪಪರಿವಿಡಿಯ ಹೆಸರು:"

#: treeview.cpp:1085
#, kde-format
msgid "New Item"
msgstr "ಹೊಸ ವಸ್ತು"

#: treeview.cpp:1085
#, kde-format
msgid "Item name:"
msgstr "ವಸ್ತುವಿನ ಹೆಸರು:"

#: treeview.cpp:1562
#, kde-format
msgid "All submenus of '%1' will be removed. Do you want to continue?"
msgstr ""

#: treeview.cpp:1563
#, fuzzy, kde-format
#| msgid "&Delete"
msgctxt "@title:window"
msgid "Delete"
msgstr "ಅಳಿ(&ಸ)ಸು"

#: treeview.cpp:1725
#, kde-format
msgid "Menu changes could not be saved because of the following problem:"
msgstr "ಈ ಕೆಳಗಿನ ತೊಂದರೆಯಿಂದಾಗಿದಾಗಿ ಪರಿವಿಡಿಯ ಬದಲಾವಣೆಗಳನ್ನು ಉಳಿಸಲಾಗಲಿಲ್ಲ:"

#: treeview.cpp:1772
#, kde-format
msgid ""
"Do you want to restore the system menu? Warning: This will remove all custom "
"menus."
msgstr ""

#: treeview.cpp:1773
#, kde-format
msgctxt "@title:window"
msgid "Restore Menu System"
msgstr ""

#~ msgid "Co&mmand:"
#~ msgstr "ಆ(&ದ)ದೇಶ:"

#~ msgid "&Place in system tray"
#~ msgstr "ಗಣಕ ವ್ಯವಸ್ಥೆಯ ತಟ್ಟೆಯಲ್ಲಿ ಇ(&ಡ)ಡು"

#, fuzzy
#~| msgid "&Name:"
#~ msgid "&Name"
#~ msgstr "(&ಹ)ಹೆಸರು:"

#~ msgid ""
#~ "<qt>The key <b>%1</b> can not be used here because it is already used to "
#~ "activate <b>%2</b>.</qt>"
#~ msgstr ""
#~ "<qt><b>%1</b> ಕೀಲಿಯನ್ನು ಉಪಯೋಗಿಸಲಾಗುವುದಿಲ್ಲ, ಏಕೆಂದರೆ ಅದನ್ನು ಈಗಾಗಲೇ <b>%2</b> "
#~ "ಅನ್ನು ಸಕ್ರಿಯಗೊಳಿಸಲು ಉಪಯೋಗಿಸಲಾಗಿದೆ.</qt>"

#~ msgid ""
#~ "<qt>The key <b>%1</b> can not be used here because it is already in use.</"
#~ "qt>"
#~ msgstr ""
#~ "<qt><b>%1</b> ಕೀಲಿಯನ್ನು ಉಪಯೋಗಿಸಲಾಗುವುದಿಲ್ಲ, ಏಕೆಂದರೆ ಅದು ಈಗಾಗಲೇ "
#~ "ಉಪಯೋಗಿಸಲ್ಪಡುತ್ತಿದೆ.</qt>"

#~ msgid "KDE control center editor"
#~ msgstr "ಕೆಡಿಇ ನಿಯಂತ್ರಣ ಕೇಂದ್ರ"

#~ msgid "KDE Control Center Editor"
#~ msgstr "ಕೆಡಿಇ ನಿಯಂತ್ರಣ ಕೇಂದ್ರ ಸಂಪಾದಕ"

#~ msgid "(C) 2000-2004, Waldo Bastian, Raffaele Sandrini, Matthias Elter"
#~ msgstr "(C) 2000-2004, Waldo Bastian, Raffaele Sandrini, Matthias Elter"

#~ msgid ""
#~ "You have made changes to the Control Center.\n"
#~ "Do you want to save the changes or discard them?"
#~ msgstr ""
#~ "ನೀವು ನಿಯಂತ್ರಣ ಕೇಂದ್ರದಲ್ಲಿ ಬದಲಾವಣೆಗಳನ್ನು ಮಾಡಿದ್ದೀರಿ.\n"
#~ "ಇವನ್ನು ನೀವು ಉಳಿಸಲು ಬಯಸುತ್ತೀರೋ ತಿರಸ್ಕರಿಸಲು ಬಯಸುತ್ತೀರೋ?"

#~ msgid "Save Control Center Changes?"
#~ msgstr "ನಿಯಂತ್ರಣ ಕೇಂದ್ರದ ಬದಲಾವಣೆಗಳನ್ನು ಉಳಿಸಲೇ?"
